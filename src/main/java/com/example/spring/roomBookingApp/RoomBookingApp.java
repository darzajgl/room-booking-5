package com.example.spring.roomBookingApp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@PropertySource("classpath:message.properties")
//@ComponentScan(basePackages = "com.example.spring.roomBookingApp.common")
public class RoomBookingApp {
    public static void main(String[] args) {
        SpringApplication.run(RoomBookingApp.class, args);
    }
}
