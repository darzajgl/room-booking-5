package com.example.spring.roomBookingApp.model;
import lombok.Data;

import javax.persistence.*;
import javax.swing.text.DateFormatter;
import java.time.LocalDate;

@Entity
public class Guest {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE) //primary key
    public static int index;

    private int guestId;
    private String name;
    private String surname;
    private boolean goldMember;
    private String checkInDate;
    private String checkOutDate;
//    private Room room;



    public Guest() {
    }

    //    public Guest(String name, String surname, boolean goldMember, LocalDate checkInDate, LocalDate checkOutDate, Room room) {
    public Guest(String name, String surname, boolean goldMember, String checkInDate, String checkOutDate) {
        super();
        this.guestId = index++;
        this.name = name;
        this.surname = surname;
        this.goldMember = goldMember;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
//        this.room = room;
    }

    public int getGuestId() {
        return guestId;
    }

    public void setGuestId(int guestId) {
        this.guestId = guestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isGoldMember() {
        return goldMember;
    }

    public void setGoldMember(boolean goldMember) {
        this.goldMember = goldMember;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    @Override
    public String toString() {
        return "Guest{" +
                "guestId=" + guestId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", goldMember=" + goldMember +
                ", checkInDate='" + checkInDate + '\'' +
                ", checkOutDate='" + checkOutDate + '\'' +
                '}';
    }


    //    public Room getRoom() {
//        return room;
//    }
//
//    public void setRoom(Room room) {
//        this.room = room;
//    }
}