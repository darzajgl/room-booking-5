package com.example.spring.roomBookingApp.controller;

import com.example.spring.roomBookingApp.model.Room;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import java.util.ArrayList;
import java.util.List;

@Controller
public class RoomController {
    private List<Room> listOfRooms;

    public RoomController() {
        listOfRooms = new ArrayList<>();
        listOfRooms.add(new Room(121, "green", 2, true));
        listOfRooms.add(new Room(122, "black", 3, false));
        listOfRooms.add(new Room(123, "red", 5, true));
    }

    @RequestMapping("/room")
    public String indexRoomGet() {
        return "room/indexRoom";
    }

    @RequestMapping(value = "/roomform", method = RequestMethod.GET)
    public ModelAndView show_room_form(Model model) {
        return new ModelAndView("room/roomform", "room", new Room());
    }

    @RequestMapping(value = "save_the_room")
    public ModelAndView save_room(@ModelAttribute(value = "room") Room room) {
        if (room.getRoomId() < 1) {
            room.setRoomId((listOfRooms.get((listOfRooms.size() - 1)).getRoomId() + 1));
            listOfRooms.add(room);
        } else {
            Room roomTemp = getRoomById(room.getRoomId());
            roomTemp.setRoomName(room.getRoomName());
            roomTemp.setCapacity(room.getCapacity());
            roomTemp.setHasWindow(room.isHasWindow());
        }

        return new ModelAndView("redirect:/viewroom");
    }

    @RequestMapping(value = "viewroom")
    public ModelAndView viewroom(Model model) {
        return new ModelAndView("room/viewroom", "listOfRooms", listOfRooms);
    }

    @RequestMapping(value = "/goBackRoom")
    public ModelAndView backRoom(@ModelAttribute(value = "room") Room room) {
        return new ModelAndView("redirect:/roomform");
    }

    @RequestMapping(value = "/deleteRoom", method = RequestMethod.POST)
    public ModelAndView deleteRoom(@RequestParam(value = "room_id") String room_id) {
        listOfRooms.remove(getRoomById(Integer.parseInt(room_id)));
        return new ModelAndView("redirect:/viewroom");
    }

    @RequestMapping(value = "/edit_room")
    public ModelAndView editRoom(@RequestParam(value = "room_id") String room_id) {
        Room roomTemp = getRoomById(Integer.parseInt(room_id));

        roomTemp.setRoomName(roomTemp.getRoomName());
        roomTemp.setRoomId(roomTemp.getRoomId());
        roomTemp.setCapacity(roomTemp.getCapacity());
        roomTemp.setHasWindow(roomTemp.isHasWindow());


        return new ModelAndView("room/roomform", "room", roomTemp);
    }

    private Room getRoomById(@RequestParam int roomId) {
        return listOfRooms.stream().filter(f -> f.getRoomId() == roomId).findFirst().get();
    }

}