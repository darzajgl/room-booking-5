INSERT INTO public.guests(guestId, name, surname, goldMember, checkInDate, checkOutDate) VALUES
(5,'Piotr', 'Drzewko',true, '2020-01-01', '2020-02-02'),
(6,'Pavel', 'Tree',false, '2001-11-11', '2001-12-12');

INSERT INTO public.rooms(roomId, roomNumber, roomName, capacity, hasWindow) VALUES
(301,321,'London',3,true),
(401,432,'Berlin',6,false);

SELECT setval('public.hibernate_sequence', 4, true);